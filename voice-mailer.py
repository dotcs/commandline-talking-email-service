#!/usr/bin/env python
# -*- coding: utf-8 -*-⌉

import os
import sys
import argparse
import smtplib, email

class VoiceMailer(object):
    def __init__(self, voicetext, voice, subject, body, receiver, sender, tempdir):
        self.voicetext = voicetext
        self.voice = voice
        self.subject = subject
        self.receiver = receiver
        self.sender_email, self.sender = sender
        self.body = body
        if not os.path.isdir(tempdir):
            raise ValueError("The given temporary directory is not valid.")
        self.tempdir = tempdir

    def configure_smtp(self, server, port, username, password):
        mailer = smtplib.SMTP_SSL(server, port)
        mailer.login(username, password)
        return mailer

    def create_voice_message(self):
        filename = os.path.join(self.tempdir, "message.aiff")
        if self.voice is not None:
            os.system("say -o %s -v %s %s" % (filename, self.voice, self.voicetext))
        else:
            os.system("say -o %s %s" % (filename, self.voicetext))
        return filename

    def send(self, mailer):
        filename = self.create_voice_message()
        msg = email.MIMEMultipart.MIMEMultipart()
        body = email.MIMEText.MIMEText(self.body)
        attachment = email.MIMEBase.MIMEBase('text', 'plain')
        attachment.set_payload(open(filename).read())
        attachment.add_header('Content-Disposition', 'attachment', filename=os.path.basename(filename))
        email.encoders.encode_base64(attachment)
        msg.attach(body)
        msg.attach(attachment)
        msg.add_header('From', self.sender)
        msg.add_header('To', self.receiver)

        mailer.sendmail(self.sender_email, [self.receiver], msg.as_string())
        mailer.close()

def main(argv):
    parser = argparse.ArgumentParser(description='')

    parser.add_argument("voicetext", type=str, help="text that should be send as a voice mail")
    parser.add_argument("-s", "--subject", type=str, help="mail subject", required=True)
    parser.add_argument("-r", "--receiver", type=str, help="receivers e-mail address", required=True)
    parser.add_argument("-b", "--body", type=str, help="mail body", required=True)
    parser.add_argument("-v", "--voice", choices=["Audrey"], default=None, help="voice provided by Apple")
    parser.add_argument("-t", "--tempdir", default=".", help="where should the soundfile temporary be saved")
    parser.add_argument("-u", "--username", type=str, help="your username", required=True)
    parser.add_argument("-p", "--password", type=str, help="your password", required=True)
    parser.add_argument("--smtp", type=str, default="smtp.googlemail.com", help="SMTP server")
    parser.add_argument("--smtp-port", type=int, default=465, help="SMTP server port")
    parser.add_argument("--sender", type=str, help="sender name", required=True)
    parser.add_argument("--sender-email", type=str, help="sender email address", required=True)

    args = parser.parse_args()

    vm = VoiceMailer(args.voicetext, args.voice, args.subject, args.body, args.receiver, (args.sender, args.sender_email), args.tempdir)
    mailer = vm.configure_smtp(args.smtp, args.smtp_port, args.username, args.password)
    vm.send(mailer)

if __name__ == "__main__":
    main(sys.argv)
